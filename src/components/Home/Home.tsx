import React from 'react';
import './Home.css';
import StartButton from "../StartButton/StartButton";
import {Redirect} from "react-router-dom";

interface HomeProps {
    authorized: boolean
}

function Home(props: HomeProps) {
    if (!props.authorized) {
        return <Redirect to={"/login"}/>
    }
    return (
        <div className="Start-container">
            <StartButton title="Изменить курс" link="/courses"/>
            <StartButton title="Создать курс" link="/editor"/>
            <StartButton title="Смотреть попытки" link="/attempts"/>
        </div>
    );
}

export default Home;

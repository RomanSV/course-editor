import React, {useState} from 'react';
import "./Login.css"
import {useHistory} from "react-router-dom";
import {login} from "../../apiUtils";

interface LoginProps {
    onLogin: () => void
}

function Login(props: LoginProps) {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const history = useHistory();

    function onSubmit(event: React.FormEvent) {
        event.preventDefault();
        login({email: email, password: password}).then(
            request => {
                if (request.status === 200) {
                    if (request.data.role !== "EDITOR" && request.data.role !== "ADMIN") {
                        alert("Извините, ваш аккаунт не имеет доступа к редактору курсов!")
                    } else {
                        localStorage.setItem("accessToken", request.data.token);
                        props.onLogin();
                        history.push("/")
                    }
                }
            }
        ).catch(e => {})
    }

    return (
        <form className="Login-form">
            <h3>Вход в систему</h3>
            <label>
                <input type="text" placeholder="Почта" size={30} value={email}
                       onChange={(e) => setEmail(e.currentTarget.value)}/>
            </label>
            <label>
                <input type="password" placeholder="Пароль" size={30} value={password}
                       onChange={(e) => setPassword(e.currentTarget.value)}/>
            </label>
            <button onClick={onSubmit}>Войти</button>
        </form>
    );
}

export default Login;

import {Redirect} from "react-router-dom";
import React, {FC, useEffect, useState} from "react";
import "./Attempts.css"
import {getAttempts} from "../../apiUtils";

interface AttemptsProps {
    authorized: boolean
}

const Attempts: FC<AttemptsProps> = ({authorized}) => {
    const [attempts, setAttempts] = useState<Array<any>>([1])
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        getAttempts().then(response => {
            if (response.status === 200) {
                setAttempts(response.data)
            } else {
                alert("Что-то пошло не так при загрузке попыток!")
                console.log(response)
            }
        })
        setLoading(false)
    }, [])

    if (!authorized) {
        return <Redirect to={"/login"}/>
    }

    if (loading) return <div>Loading...</div>

    const attemptsList = attempts.map((a, index) => <div key={index} className="Attempt">
        <div className="Attempt-label">Пользователь:</div>
        <div className="Attempt-value">{a.userId}</div>
        <div className="Attempt-label">Задание:</div>
        <div className="Attempt-value">{a.taskId}</div>
        <div className="Attempt-label">Аудио:</div>
        <a className="Attempt-value" href={a.audio}>{a.audio}</a>
        <div style={{"display": "flex"}}>
        <div className="Attempt-label">F0:</div>
        <a className="Attempt-value" href={a.pitch}>{a.pitch}</a>
        </div>
        <div className="Attempt-label">Время:</div>
        <div className="Attempt-value">{a.timestamp}</div>
        <div className="Attempt-label">Версия приложения:</div>
        <div className="Attempt-value">{a.appVersion}</div>
    </div>)
    return (
        <div className="Attempts-container">
            {attemptsList}
        </div>
    );
}

export default Attempts;

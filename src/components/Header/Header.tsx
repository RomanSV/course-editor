import React, {FC} from "react";
import "./Header.css"
import {Link} from "react-router-dom";

interface HeaderProps {
    authorized: boolean
    username: string
}

const Header: FC<HeaderProps> = ({authorized, username}) => {
    if (!authorized) {
        return <header className="App-header"> CourseEditor </header>
    }
    return (
        <header className="App-header">
            <Link className="App-header-logo" to="/">CourseEditor</Link>
            <Link className="App-header-user" to="/account">{username}</Link>
        </header>
    );
}

export default Header;

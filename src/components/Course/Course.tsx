import React from 'react';
import './Course.css';
import {deleteCourse} from "../../apiUtils";

interface CourseProps {
    id: number
    name: string
    category: string
    version: string
}

function Course(props: CourseProps) {

    const onDeleteCourse = (event: React.MouseEvent<HTMLElement>) => {
        event.stopPropagation();
        event.preventDefault();
        deleteCourse(props.id).then((response) => {
            if (response.status !== 204) {
                alert("При удалении курса возникла ошибка!");
                console.log(response);
            } else {
                window.location.reload();
            }
        })
    }

    return (
        <div className="Course">
            <h3>{props.name}</h3>
            <p/>
            <div>Категория: {props.category}</div>
            <div>Версия: {props.version}</div>
            <button onClick={onDeleteCourse}>Удалить курс</button>
        </div>
    );
}

export default Course;

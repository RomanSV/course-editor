import './Courses.css';
import Course from "../Course/Course";
import {getCourses} from "../../apiUtils";
import {Link, Redirect} from "react-router-dom";
import {useQuery} from "react-query";
import CourseModel from "../../model/CourseModel";

interface CoursesProps {
    authorized: boolean
}

function Courses(props: CoursesProps) {
    const { isLoading, error, data } = useQuery("courses", getCourses, {enabled: props.authorized})

    if (!props.authorized) return <Redirect to={"/login"}/>;

    if (error) return <div>Error!</div>;

    if (isLoading) return <div>Loading...</div>;

    const courses = data.map((c: CourseModel, i: number) => {
        return (
            <Link key={"c" + i} to={"/editor?c=" + c.id}>
                <Course id={c.id} name={c.title} category={c.category} version={c.version}/>
            </Link>
        )
    })
    return (
        <div className="Courses">
            <h2>Выберите курс для редактирования:</h2>
            <div className="Courses-container">
                {courses}
            </div>
        </div>
    );
}

export default Courses;

import React, {useEffect, useState} from 'react';
import "./Editor.css"
import EditCourse from "./EditCourse";
import EditLessons from "./EditLessons";
import EditTasks from "./EditTasks";
import CourseModel from "../../model/CourseModel";
import LessonModel from "../../model/LessonModel";
import TaskModel from "../../model/TaskModel";
import {Link, Redirect, useHistory, useLocation} from "react-router-dom";
import {
    createCourse,
    createLesson,
    createTask,
    getCourse,
    getLessons,
    updateCourse,
    updateLesson,
    updateTask
} from "../../apiUtils";

interface EditorProps {
    authorized: boolean
}

function Editor(props: EditorProps) {

    const date = new Date();
    const location = useLocation();
    const history = useHistory();
    const [step, setStep] = useState(0);
    const [courseData, setCourseData] = useState<CourseModel>({
        title: "", description: "", difficulty: 0, logo: "",
        category: "", active: false, version: "1",
        language: "", authors: [], id: -1, releaseDate: date.toISOString()
    });
    const [lessonsData, setLessonsData] = useState<Array<LessonModel>>([])
    const [tasksData, setTasksData] = useState<Array<TaskModel>>([])
    const [isNew, setNew] = useState(false)

    useEffect(() => {
        if (!props.authorized) return;
        if (location.search.length !== 0) {
            const courseId = location.search.split("=")[1]
            getCourse(courseId).then(response => {
                if (response.status === 200) {
                    const data = response.data
                    const course: CourseModel = {
                        id: data.id,
                        title: data.title,
                        description: data.description,
                        logo: data.logo,
                        language: data.language,
                        difficulty: data.difficulty,
                        category: data.category,
                        authors: data.authors,
                        active: data.active,
                        releaseDate: data.releaseDate,
                        version: data.version,
                    }
                    setCourseData(course);
                }
            });
            getLessons(courseId).then(response => {
                if (response.status === 200) {
                    let tasks: Array<TaskModel> = []
                    const lessons: Array<LessonModel> = response.data.map((l: any) => {
                        tasks = tasks.concat(l.tasks.map((t: any) => {
                            return {
                                id: t.id,
                                lessonId: l.id,
                                orderNumber: t.orderNumber,
                                instructions: t.instructions,
                                text: t.text,
                                transcription: t.transcription,
                                modelType: t.modelType,
                                model: t.model,
                                pitch: t.pitch,
                                markup: t.markup
                            }
                        }));
                        return {
                            id: l.id,
                            title: l.title,
                            description: l.description,
                            orderNumber: l.orderNumber,
                            duration: l.duration
                        }
                    });
                    setTasksData(tasks);
                    setLessonsData(lessons);
                }
            });
            setNew(false)
        } else {
            setNew(true)
        }
    }, [props, location])

    if (!props.authorized) {
        return <Redirect to={"/login"}/>
    }

    function handleUpdateCourse(course: CourseModel) {
        setCourseData(course)
    }

    function handleUpdateLessons(lessons: Array<LessonModel>) {
        setLessonsData(lessons)
    }

    function handleUpdateTasks(tasks: Array<TaskModel>) {
        setTasksData(tasks)
    }

    function handleNext() {
        if (step === 0) {
            if (isNew) {
                createCourse({
                    title: courseData.title,
                    description: courseData.description,
                    logo: courseData.logo,
                    language: courseData.language,
                    difficulty: courseData.difficulty,
                    category: courseData.category,
                    authors: courseData.authors,
                    active: courseData.active,
                    releaseDate: courseData.releaseDate,
                    version: courseData.version
                }).then(response => {
                    if (response.status === 201) {
                        const idString = response.headers.location?.split("/").pop() ?? "-1";
                        const newId = parseInt(idString);
                        setCourseData({
                            ...courseData,
                            id: newId
                        });
                    } else {
                        alert("Что-то пошло не так при создании курса!")
                    }
                })
            } else {
                const courseId = location.search.split("=")[1]
                updateCourse(courseId, {
                    title: courseData.title,
                    description: courseData.description,
                    logo: courseData.logo,
                    language: courseData.language,
                    difficulty: courseData.difficulty,
                    category: courseData.category,
                    authors: courseData.authors,
                    active: courseData.active,
                    releaseDate: courseData.releaseDate,
                    version: courseData.version
                }).then(response => {
                    if (response.status !== 200) {
                        alert("Что-то пошло не так при обновлении курса!")
                    }
                })
            }
            setStep(step + 1)
        } else if (step === 1) {
            if (lessonsData.length === 0) {
                alert("Чтобы продолжить, создайте хотя бы один урок!")
            } else {
                lessonsData.forEach(lesson => {
                    if (lesson.id === -1) {
                        createLesson({
                            courseId: courseData.id,
                            orderNumber: lesson.orderNumber,
                            title: lesson.title,
                            description: lesson.description,
                            duration: lesson.duration
                        }).then((response) => {
                            if (response.status === 201) {
                                const idString = response.headers.location?.split("/").pop() ?? "-1";
                                const newId = parseInt(idString);
                                const newLessons = lessonsData.map((lesson) => {
                                    if (lesson.id === -1) {
                                        return {
                                            ...lesson,
                                            id: newId
                                        }
                                    } else {
                                        return lesson
                                    }
                                })
                                setLessonsData(newLessons)
                            } else {
                                alert("Что-то пошло не так при создании урока")
                            }
                        }).catch(() => alert("Что-то пошло не так при создании урока! " +
                            "Проверьте, что номера уроков не повторяются!"))
                    } else {
                        updateLesson(lesson.id, {
                            courseId: courseData.id,
                            orderNumber: lesson.orderNumber,
                            title: lesson.title,
                            description: lesson.description,
                            duration: lesson.duration
                        }).then((response) => {
                            if (response.status !== 200) {
                                alert("Что-то пошло не так при обновлении уроков!");
                                console.log(response);
                            }
                        }).catch(() => alert("Что-то пошло не так при создании урока! " +
                            "Проверьте, что номера уроков не повторяются!"))
                    }
                })
                setStep(step + 1)
            }
        }
    }

    function handleFinish() {
        if (step === 2) {
            tasksData.forEach(task => {
                if (task.id === -1) {
                    createTask({
                        lessonId: task.lessonId,
                        orderNumber: task.orderNumber,
                        instructions: task.instructions,
                        text: task.text,
                        transcription: task.transcription,
                        modelType: task.modelType,
                        model: task.model,
                        pitch: task.pitch,
                        markup: task.markup
                    }).then((response) => {
                        if (response.status === 201) {
                            const idString = response.headers.location?.split("/").pop() ?? "-1";
                            const newId = parseInt(idString);
                            const newTasks = tasksData.map((task) => {
                                if (task.id === -1) {
                                    return {
                                        ...task,
                                        id: newId
                                    }
                                } else {
                                    return task
                                }
                            })
                            setTasksData(newTasks)
                        } else {
                            alert("Что-то пошло не так при создании задания")
                        }
                    }).catch(() => alert("Что-то пошло не так при создании задания! " +
                        "Проверьте, что номера заданий не повторяются в рамках урока!"))
                } else {
                    updateTask(task.id, {
                        lessonId: task.lessonId,
                        orderNumber: task.orderNumber,
                        instructions: task.instructions,
                        text: task.text,
                        transcription: task.transcription,
                        modelType: task.modelType,
                        model: task.model,
                        pitch: task.pitch,
                        markup: task.markup
                    }).then((response) => {
                        if (response.status !== 200) {
                            alert("Что-то пошло не так при обновлении заданий!");
                            console.log(response);
                        }
                    }).catch(() => alert("Что-то пошло не так при создании урока! " +
                        "Проверьте, что номера уроков не повторяются!"))
                }
            })
        }
        history.push("/")
    }

    function handleBack() {
        setStep(step - 1)
    }

    const formSwitch = () => {
        switch (step) {
            case 0:
                return <EditCourse courseData={courseData} onCourseEdit={handleUpdateCourse} isNew={isNew}/>
            case 1:
                return <EditLessons lessonsData={lessonsData} onLessonsEdit={handleUpdateLessons}/>
            case 2:
                return <EditTasks tasksData={tasksData} lessonNumbers={lessonsData.map(l => [l.orderNumber, l.id])}
                                  onTasksEdit={handleUpdateTasks}/>
            default:
                return <div>Oopsie!</div>
        }
    }

    return (
        <div className="Editor">
            {formSwitch()}
            {step > 0 && <button onClick={handleBack}>Назад</button>}
            {step === 0 && <Link to="/"> <button>Отменить</button> </Link>}
            {step < 2 && <button onClick={handleNext}>Дальше</button>}
            {step === 2 && <button onClick={handleFinish}>Завершить</button>}
        </div>
    );
}

export default Editor;

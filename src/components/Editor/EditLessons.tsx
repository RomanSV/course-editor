import React, {useEffect, useState} from 'react';
import LessonModel from "../../model/LessonModel";
import EditLesson from "./EditLesson";
import {deleteLesson} from "../../apiUtils";

interface EditLessonsProps {
    lessonsData: Array<LessonModel>;
    onLessonsEdit: (lessons: Array<LessonModel>) => void;
}

function EditLessons(props: EditLessonsProps) {
    const [lessonsData, setLessonsData] = useState(props.lessonsData)

    useEffect(() => {
        props.onLessonsEdit(lessonsData)
    }, [props, lessonsData])

    function handleAddLesson() {
        setLessonsData([...lessonsData, {
            id: -1, title: "", description: "", orderNumber: 0, duration: 0
        }]);
    }

    function onLessonEdit(id: number, newLesson: LessonModel) {
        const newLessons = lessonsData.map((lesson, index) => {
            if (index === id) {
                return newLesson
            } else {
                return lesson
            }
        })
        setLessonsData(newLessons)
    }

    function handleRemoveLesson(event: React.MouseEvent<HTMLElement>) {
        event.stopPropagation();
        if (lessonsData[parseInt(event.currentTarget.id)].id !== -1) {
            deleteLesson(lessonsData[parseInt(event.currentTarget.id)].id).then(
                (response) => {
                    if (response.status !== 204) {
                        alert("Что-то пошло не так при удалении урока!")
                        console.log(response)
                    }
                }
            )
        }
        const tmp = [...lessonsData]
        tmp.splice(parseInt(event.currentTarget.id), 1)
        setLessonsData(tmp);
    }

    const lessons = lessonsData.map((lesson, index) => {
        return (
            <div key={"l" + index} className="Edit-list-item">
                <EditLesson id={index} lessonData={lesson} onLessonEdit={onLessonEdit}/>
                <button id={index.toString()} className="Delete-button" onClick={handleRemoveLesson}>
                    X
                </button>
            </div>
        )
    });

    return (
        <div>
            <h4>Заполните информацию об уроках:</h4>
            <div className="Edit-list">
                {lessons}
            </div>
            <button onClick={handleAddLesson}>Новый урок</button>
        </div>
    );
}

export default EditLessons
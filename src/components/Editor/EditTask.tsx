import React, {FC} from 'react';
import TaskModel from "../../model/TaskModel";

interface EditTaskProps {
    id: number;
    taskData: TaskModel;
    lessonNumbers: Array<Array<number>>;
    onTaskEdit: (id: number, task: TaskModel) => void;
}

const EditTask: FC<EditTaskProps> = ({id, taskData, lessonNumbers, onTaskEdit}) => {
    const setLessonId = (value: number) => {
        onTaskEdit(id, {...taskData, lessonId: value});
    }
    const setOrderNumber = (value: number) => {
        onTaskEdit(id, {...taskData, orderNumber: value});
    }
    const setInstructions = (value: string) => {
        onTaskEdit(id, {...taskData, instructions: value});
    }
    const setText = (value: string) => {
        onTaskEdit(id, {...taskData, text: value});
    }
    const setTranscription = (value: string) => {
        onTaskEdit(id, {...taskData, transcription: value});
    }
    const setModelType = (value: string) => {
        onTaskEdit(id, {...taskData, modelType: value});
    }

    const setModel = (value: string) => {
        onTaskEdit(id, {...taskData, model: value});
    }

    const setPitch = (value: string) => {
        onTaskEdit(id, {...taskData, pitch: value});
    }

    const lessonsOptions = lessonNumbers.map((value) => {
        return <option key={value[1]} value={value[1]}>{value[0]}</option>
    })

    return (
        <form key={id} className="Edit-step Tasks-step">
            <label>
                <text>Номер:</text>
                <input type="number" pattern="[0-9]*" size={3}
                       value={taskData.orderNumber}
                       onChange={e => setOrderNumber(parseInt(e.currentTarget.value))}/>
            </label>
            <label>
                <text>Урок:</text>
                <select value={taskData.lessonId}
                        onChange={e => setLessonId(parseInt(e.currentTarget.value))}>
                    {lessonsOptions}
                </select>
            </label>
            <label>
                <text>Инструкции:</text>
                <input type="text" value={taskData.instructions} size={35}
                       onChange={e => setInstructions(e.currentTarget.value)}/>
            </label>
            <label>
                <text>Текст:</text>
                <input type="text" value={taskData.text}
                       onChange={e => setText(e.currentTarget.value)}/>
            </label>
            <label>
                <text>Транскрипция:</text>
                <input type="text" value={taskData.transcription}
                       onChange={e => setTranscription(e.currentTarget.value)}/>
            </label>
            <label>
                <text>Тип модели:</text>
                <select value={taskData.modelType} onChange={e => setModelType(e.currentTarget.value)}>
                    <option value="AUDIO">Аудио</option>
                    <option value="VIDEO">Видео</option>
                </select>
            </label>
            <label>
                <text>Модель:</text>
                <input type="text" value={taskData.model} size={50}
                       onChange={e => setModel(e.currentTarget.value)}/>
            </label>
            <label>
                <text>F0:</text>
                <input type="text" value={taskData.pitch} size={50}
                       onChange={e => setPitch(e.currentTarget.value)}/>
            </label>
            <label>
                <text>Разметка:</text>
                <input type="text" value={JSON.stringify(taskData.markup)} size={50} readOnly/>
            </label>
        </form>
    );
}

export default EditTask
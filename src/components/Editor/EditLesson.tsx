import React, {FC} from 'react';
import LessonModel from "../../model/LessonModel";

interface EditLessonProps {
    id: number;
    lessonData: LessonModel;
    onLessonEdit: (id: number, lesson: LessonModel) => void;
}

const EditLesson: FC<EditLessonProps> = ({id, lessonData, onLessonEdit}) => {
    const setTitle = (value: string) => {
        onLessonEdit(id, {...lessonData, title: value});
    }
    const setDescription = (value: string) => {
        onLessonEdit(id, {...lessonData, description: value});
    }
    const setOrderNumber = (value: number) => {
        onLessonEdit(id, {...lessonData, orderNumber: value});
    }
    const setDuration = (value: number) => {
        onLessonEdit(id, {...lessonData, duration: value});
    }

    return (
        <form key={id} className="Edit-step">
            <label>
                <text>Номер:</text>
                <input type="number" pattern="[0-9]*" size={3}
                       value={lessonData.orderNumber}
                       onChange={e => setOrderNumber(parseInt(e.currentTarget.value))}/>
            </label>
            <label>
                <text>Название:</text>
                <input type="text" value={lessonData.title}
                       onChange={e => setTitle(e.currentTarget.value)}
                size={30}/>
            </label>
            <label>
                <text>Описание:</text>
                <textarea value={lessonData.description}
                          onChange={e => setDescription(e.currentTarget.value)}
                          cols={50} rows={3}/>
            </label>
            <label>
                <text>Длительность (в минутах):</text>
                <input type="number" pattern="[0-9]*" size={4}
                       value={lessonData.duration}
                       onChange={e => setDuration(parseInt(e.currentTarget.value))}/>
            </label>
        </form>
    );
}

export default EditLesson
import React, {useEffect, useState} from 'react';
import EditTask from "./EditTask";
import TaskModel from "../../model/TaskModel";
import taskModel from "../../model/TaskModel";
import {deleteTask} from "../../apiUtils";

interface EditTasksProps {
    tasksData: Array<TaskModel>;
    lessonNumbers: Array<Array<number>>;
    onTasksEdit: (tasks: Array<TaskModel>) => void;
}

function EditTasks(props: EditTasksProps) {
    const [tasksData, setTasksData] = useState(props.tasksData)

    useEffect(() => {
        props.onTasksEdit(tasksData)
    }, [props, tasksData])

    function handleAddTask() {
        setTasksData([...tasksData, {
            id: -1, lessonId: props.lessonNumbers[0][1], orderNumber: 0, instructions: "", text: "",
            transcription: "", modelType: "AUDIO", model: "", pitch: "", markup: []
        }]);
    }

    function handleRemoveTask(event: React.MouseEvent<HTMLElement>) {
        event.stopPropagation();
        if (tasksData[parseInt(event.currentTarget.id)].id !== -1) {
            deleteTask(tasksData[parseInt(event.currentTarget.id)].id).then(
                (response) => {
                    if (response.status !== 204) {
                        alert("Что-то пошло не так при удалении задания!")
                        console.log(response)
                    }
                }
            )
        }
        const tmp = [...tasksData]
        tmp.splice(parseInt(event.currentTarget.id), 1)
        setTasksData(tmp);
    }

    function onTaskEdit(id: number, newTask: taskModel) {
        const newLessons = tasksData.map((task, index) => {
            if (index === id) {
                return newTask
            } else {
                return task
            }
        })
        setTasksData(newLessons)
    }

    const tasks = tasksData.map((task, index) => {
        return (
            <div key={"t" + index} className="Edit-list-item">
                <EditTask id={index} taskData={task} lessonNumbers={props.lessonNumbers} onTaskEdit={onTaskEdit}/>
                <button id={index.toString()} className="Delete-button" onClick={handleRemoveTask}>X</button>
            </div>
        )
    });

    return (
        <div>
            <h4>Заполните информацию о заданиях:</h4>
            <div className="Edit-list">
                {tasks}
            </div>
            <button onClick={handleAddTask}>Новое задание</button>
        </div>
    );
}

export default EditTasks
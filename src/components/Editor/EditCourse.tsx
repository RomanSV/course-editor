import React, {FC} from 'react';
import CourseModel from "../../model/CourseModel";

interface EditCourseProps {
    courseData: CourseModel;
    isNew: Boolean;
    onCourseEdit: (course: CourseModel) => void;
}

const EditCourse: FC<EditCourseProps> = ({courseData, isNew, onCourseEdit}) => {

    const setTitle = (value: string) => {
        onCourseEdit({...courseData, title: value});
    }
    const setDescription = (value: string) => {
        onCourseEdit({...courseData, description: value});
    }
    const setLogo = (value: string) => {
        onCourseEdit({...courseData, logo: value});
    }
    const setDifficulty = (value: number) => {
        onCourseEdit({...courseData, difficulty: value});
    }
    const setLanguage = (value: string) => {
        onCourseEdit({...courseData, language: value});
    }
    const setCategory = (value: string) => {
        onCourseEdit({...courseData, category: value});
    }
    const setAuthors = (value: Array<string>) => {
        onCourseEdit({...courseData, authors: value});
    }
    const setActive = (value: boolean) => {
        onCourseEdit({...courseData, active: value});
    }

    return (
        <div>
            <h4>Заполните информацию о курсе:</h4>
            <form className="Edit-step">
                <label>
                    <text>Название:</text>
                    <input type="text" value={courseData.title}
                           onChange={e => setTitle(e.currentTarget.value)}/>
                </label>
                <label>
                    <text>Описание:</text>
                    <textarea value={courseData.description}
                              onChange={e => setDescription(e.currentTarget.value)}
                              cols={30} rows={8}/>
                </label>
                <label>
                    <text>Картинка (url):</text>
                    <input type="text" value={courseData.logo} size={35}
                           onChange={e => setLogo(e.currentTarget.value)}/>
                </label>
                <label>
                    <text>Язык:</text>
                    <select value={courseData.language} onChange={e => setLanguage(e.currentTarget.value)}>
                        <option value="EN_EN">Английский</option>
                        <option value="EN_GB">Английский (Брит.)</option>
                        <option value="EN_US">Английский (Амер.)</option>
                        <option value="VI_VN">Вьетнамский</option>
                        <option value="ZH_CH">Китайский</option>
                        <option value="DE_DE">Немецкий</option>
                        <option value="RU_RU">Русский</option>
                        <option value="FR_FR">Французский</option>
                        <option value="JA_JP">Японский</option>
                    </select>
                </label>
                <label>
                    <text>Сложность:</text>
                    <input type="number" pattern="[0-9]*" min={0} max={10} value={courseData.difficulty}
                           onChange={e => setDifficulty(parseInt(e.currentTarget.value))}/>
                </label>
                <label>
                    <text>Категория:</text>
                    <input type="text" value={courseData.category}
                           onChange={e => setCategory(e.currentTarget.value)}/>
                </label>
                <label>
                    <text>Авторы:</text>
                    <input type="text" value={courseData.authors.join(", ")} size={35}
                           onChange={e => setAuthors(e.currentTarget.value.split(", "))}/>
                </label>
                {
                    isNew === true ||
                    <label>
                        <text>Скрыт из выдачи:</text>
                        <input type="checkbox" checked={!courseData.active}
                               onChange={e => setActive(!e.currentTarget.checked)}/>
                    </label>
                }
            </form>
        </div>
    );
}

export default EditCourse
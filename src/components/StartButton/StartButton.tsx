import React from 'react';
import './StartButton.css'
import {Link} from "react-router-dom";

interface StartButtonProps {
    title: string
    link: string
}

function StartButton(props: StartButtonProps) {
    return (
        <Link to={props.link}>
            <div className="StartButton">
                {props.title}
            </div>
        </Link>
    );
}

export default StartButton
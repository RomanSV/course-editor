import {Redirect} from "react-router-dom";
import React, {FC, useEffect, useState} from "react";
import {getAccount} from "../../apiUtils";
import "./Account.css"

interface AccountProps {
    authorized: boolean
}

const Account: FC<AccountProps> = ({authorized}) => {
    const [loading, setLoading] = useState(true)
    const [displayName, setDisplayName] = useState("")
    const [gender, setGender] = useState("")
    const [nativeLanguage, setNativeLanguage] = useState("")
    const [birthYear, setBirthYear] = useState(0)
    const [allowDataSharing, setAllowDataSharing] = useState(false)

    const genderMapping = new Map([
        ["MALE", "Мужской"],
        ["FEMALE", "Женский"]
    ]);

    const languageMapping = new Map([
        ["EN_EN", "Английский"],
        ["EN_GB", "Английский (Брит.)"],
        ["EN_US", "Английский (Амер.)"],
        ["VI_VN", "Вьетнамский"],
        ["ZH_CH", "Китайский"],
        ["DE_DE", "Немецкий"],
        ["RU_RU", "Русский"],
        ["FR_FR", "Французский"],
        ["JA_JP", "Японский"],
    ]);

    useEffect(() => {
        getAccount().then(response => {
                if (response.status === 200) {
                    setDisplayName(response.data.displayName)
                    setGender(response.data.gender)
                    setBirthYear(response.data.birthYear)
                    setNativeLanguage(response.data.nativeLanguage)
                    setAllowDataSharing(response.data.sendAttempts)
                } else {
                    console.log(response)
                }
                setLoading(false)
            }
        )
    }, [])
    if (!authorized) {
        return <Redirect to={"/login"}/>
    }

    if (loading) return <div>Loading...</div>
    return (
        <div className="Account-container">
            <div className="Account-property">
                <div className="Account-property-label">Псевдоним</div>
                <div className="Account-property-value">{displayName}</div>
            </div>
            <div className="Account-property">
                <div className="Account-property-label">Пол</div>
                <div className="Account-property-value">{genderMapping.get(gender)}</div>
            </div>
            <div className="Account-property">
                <div className="Account-property-label">Родной язык</div>
                <div className="Account-property-value">{languageMapping.get(nativeLanguage)}</div>
            </div>
            <div className="Account-property">
                <div className="Account-property-label">Год рождения</div>
                <div className="Account-property-value">{birthYear}</div>
            </div>
            <div className="Account-property">
                <div className="Account-property-label">Сбор попыток разрешён</div>
                <div className="Account-property-value">{allowDataSharing ? "Да" : "Нет"}</div>
            </div>
        </div>
    );
}

export default Account;

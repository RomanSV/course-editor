import React, {useEffect, useState} from 'react';
import './App.css';
import Home from "../Home/Home";
import {HashRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import Editor from "../Editor/Editor";
import Courses from "../Courses/Courses";
import Login from "../Login/Login";
import Header from "../Header/Header";
import {getAccount} from "../../apiUtils";
import Account from "../Account/Account";
import Attempts from "../Attempts/Attempts";

function App() {
    const [authorized, setAuthorized] = useState(false)
    const [loading, setLoading] = useState(true)
    const [displayName, setDisplayName] = useState("")


    useEffect(() => {
        if (localStorage.getItem("accessToken") != null) {
            getAccount().then(response => {
                    if (response.status === 200) {
                        setAuthorized(true)
                        setDisplayName(response.data.displayName)
                    } else {
                        setAuthorized(false)
                        console.log(response)
                    }
                    setLoading(false)
                }
            )
        } else {
            setAuthorized(false)
            setLoading(false)
        }
    }, [])

    function onLogin() {
        setLoading(true);
        getAccount().then(response => {
                if (response.status === 200) {
                    setAuthorized(true)
                    setDisplayName(response.data.displayName)
                } else {
                    setAuthorized(false)
                    console.log(response)
                }
                setLoading(false)
            }
        )
    }

    if (loading) return <div>Loading...</div>

    return (
        <Router>
            <div className="App">
                <Header authorized={authorized} username={displayName}/>
                <Switch>
                    <Route exact path="/" component={() => <Home authorized={authorized}/>}/>
                    <Route exact path="/editor" component={() => <Editor authorized={authorized}/>}/>
                    <Route exact path="/courses" component={() => <Courses authorized={authorized}/>}/>
                    <Route exact path="/account" component={() => <Account authorized={authorized}/>}/>
                    <Route exact path="/attempts" component={() => <Attempts authorized={authorized}/>}/>
                    <Route exact path="/login" component={() => <Login onLogin={onLogin}/>}/>
                    <Redirect from="*" to="/"/>
                </Switch>
            </div>
        </Router>
    );
}

export default App;

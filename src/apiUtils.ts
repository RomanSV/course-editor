import {CourseRequest, LessonRequest, LoginRequest, TaskRequest} from "./model/Requests";
import axios from "axios";

const API_BASE_URL = "http://localhost:8080"

const config = () => localStorage.getItem("accessToken") ? {
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + localStorage.getItem("accessToken")
    }
} : {
    headers: {
        "Content-Type": "application/json",
    }
}

const request = (options: any) => {
    const headers = new Headers({
        "Content-Type": "application/json"
    });

    if (localStorage.getItem("accessToken")) {
        headers.append("Authorization", "Bearer " + localStorage.getItem("accessToken"));
    }

    let fetchOpts: RequestInit = {
        headers: headers,
        method: options.method
    }
    if (options.body) {
        fetchOpts = {
            ...fetchOpts,
            body: options.body
        }
    }

    return fetch(options.url, fetchOpts)
        .then(response => {
                console.log(response);
                return response.json().then(json => {
                    if (!response.ok) {
                        return Promise.reject(json);
                    }
                    return json;
                })
            }
        );
};

export function login(loginRequest: LoginRequest) {
    return axios.post(API_BASE_URL + "/login", loginRequest, config())
}

export function getCourses() {
    return request({
        url: API_BASE_URL + "/courses",
        method: "GET"
    })
}

export function getCourse(id: number | string) {
    return axios.get(API_BASE_URL + "/courses/" + id, config())
}

export function updateCourse(id: number | string, courseRequest: CourseRequest) {
    return axios.put(API_BASE_URL + "/courses/" + id, courseRequest, config());
}

export function createCourse(courseRequest: CourseRequest) {
    return axios.post(API_BASE_URL + "/courses", courseRequest, config())
}

export function deleteCourse(courseId: number) {
    return axios.delete(API_BASE_URL + "/courses/" + courseId, config())
}

export function getLessons(courseId: number | string) {
    return axios.get(API_BASE_URL + "/lessons?course=" + courseId)
}

export function updateLesson(id: number | string, lessonRequest: LessonRequest) {
    return axios.put(API_BASE_URL + "/lessons/" + id, lessonRequest, config());
}

export function createLesson(lessonRequest: LessonRequest) {
    return axios.post(API_BASE_URL + "/lessons", lessonRequest, config());
}

export function deleteLesson(id: number | string) {
    return axios.delete(API_BASE_URL + "/lessons/" + id, config())
}

export function updateTask(id: number | string, taskRequest: TaskRequest) {
    return axios.put(API_BASE_URL + "/tasks/" + id, taskRequest, config());
}

export function createTask(taskRequest: TaskRequest) {
    return axios.post(API_BASE_URL + "/tasks", taskRequest, config());
}

export function deleteTask(id: number | string) {
    return axios.delete(API_BASE_URL + "/tasks/" + id, config())
}

export function getAccount() {
    return axios.get(API_BASE_URL + "/account", config())
}

export function getAttempts() {
    return axios.get(API_BASE_URL + "/attempts", config())
}
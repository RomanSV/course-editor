interface TaskModel {
    id: number;
    lessonId: number;
    orderNumber: number;
    instructions: string;
    text: string;
    transcription: string;
    modelType: string;
    model: string;
    pitch: string;
    markup: any;
}

export default TaskModel
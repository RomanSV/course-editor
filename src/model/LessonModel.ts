interface LessonModel {
    id: number;
    title: string;
    description: string;
    orderNumber: number;
    duration: number;
}

export default LessonModel
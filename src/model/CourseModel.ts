interface CourseModel {
    id: number;
    title: string;
    description: string;
    logo: string;
    language: string;
    difficulty: number;
    category: string;
    authors: Array<string>;
    active: boolean;
    releaseDate: string;
    version: string;
}

export default CourseModel
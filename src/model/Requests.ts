export interface LoginRequest {
    email: string;
    password: string
}

export interface CourseRequest {
    title: string,
    description: string,
    logo: string,
    language: string,
    difficulty: number,
    category: string,
    authors: Array<string>,
    active: boolean,
    releaseDate: string,
    version: string
}
export interface LessonRequest {
    courseId: number,
    orderNumber: number,
    title: string,
    description: string,
    duration: number
}

export interface TaskRequest {
    lessonId: number;
    orderNumber: number;
    instructions: string;
    text: string;
    transcription: string;
    modelType: string;
    model: string;
    pitch: string;
    markup: any;
}